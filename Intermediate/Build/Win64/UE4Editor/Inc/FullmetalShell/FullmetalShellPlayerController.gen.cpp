// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FullmetalShell/Game/FullmetalShellPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFullmetalShellPlayerController() {}
// Cross Module References
	FULLMETALSHELL_API UClass* Z_Construct_UClass_AFullmetalShellPlayerController_NoRegister();
	FULLMETALSHELL_API UClass* Z_Construct_UClass_AFullmetalShellPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_FullmetalShell();
// End Cross Module References
	void AFullmetalShellPlayerController::StaticRegisterNativesAFullmetalShellPlayerController()
	{
	}
	UClass* Z_Construct_UClass_AFullmetalShellPlayerController_NoRegister()
	{
		return AFullmetalShellPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AFullmetalShellPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFullmetalShellPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_FullmetalShell,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFullmetalShellPlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Game/FullmetalShellPlayerController.h" },
		{ "ModuleRelativePath", "Game/FullmetalShellPlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFullmetalShellPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFullmetalShellPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFullmetalShellPlayerController_Statics::ClassParams = {
		&AFullmetalShellPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFullmetalShellPlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFullmetalShellPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFullmetalShellPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFullmetalShellPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFullmetalShellPlayerController, 1344397125);
	template<> FULLMETALSHELL_API UClass* StaticClass<AFullmetalShellPlayerController>()
	{
		return AFullmetalShellPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFullmetalShellPlayerController(Z_Construct_UClass_AFullmetalShellPlayerController, &AFullmetalShellPlayerController::StaticClass, TEXT("/Script/FullmetalShell"), TEXT("AFullmetalShellPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFullmetalShellPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
