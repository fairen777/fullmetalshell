// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FULLMETALSHELL_FullmetalShellGameMode_generated_h
#error "FullmetalShellGameMode.generated.h already included, missing '#pragma once' in FullmetalShellGameMode.h"
#endif
#define FULLMETALSHELL_FullmetalShellGameMode_generated_h

#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_SPARSE_DATA
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_RPC_WRAPPERS
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFullmetalShellGameMode(); \
	friend struct Z_Construct_UClass_AFullmetalShellGameMode_Statics; \
public: \
	DECLARE_CLASS(AFullmetalShellGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FullmetalShell"), FULLMETALSHELL_API) \
	DECLARE_SERIALIZER(AFullmetalShellGameMode)


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFullmetalShellGameMode(); \
	friend struct Z_Construct_UClass_AFullmetalShellGameMode_Statics; \
public: \
	DECLARE_CLASS(AFullmetalShellGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FullmetalShell"), FULLMETALSHELL_API) \
	DECLARE_SERIALIZER(AFullmetalShellGameMode)


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FULLMETALSHELL_API AFullmetalShellGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFullmetalShellGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FULLMETALSHELL_API, AFullmetalShellGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFullmetalShellGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FULLMETALSHELL_API AFullmetalShellGameMode(AFullmetalShellGameMode&&); \
	FULLMETALSHELL_API AFullmetalShellGameMode(const AFullmetalShellGameMode&); \
public:


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FULLMETALSHELL_API AFullmetalShellGameMode(AFullmetalShellGameMode&&); \
	FULLMETALSHELL_API AFullmetalShellGameMode(const AFullmetalShellGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FULLMETALSHELL_API, AFullmetalShellGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFullmetalShellGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFullmetalShellGameMode)


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_9_PROLOG
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_SPARSE_DATA \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_RPC_WRAPPERS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_INCLASS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_SPARSE_DATA \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FULLMETALSHELL_API UClass* StaticClass<class AFullmetalShellGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FullmetalShell_Source_FullmetalShell_Game_FullmetalShellGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
