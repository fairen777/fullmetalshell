// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FULLMETALSHELL_FullmetalShellPlayerController_generated_h
#error "FullmetalShellPlayerController.generated.h already included, missing '#pragma once' in FullmetalShellPlayerController.h"
#endif
#define FULLMETALSHELL_FullmetalShellPlayerController_generated_h

#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_SPARSE_DATA
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_RPC_WRAPPERS
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFullmetalShellPlayerController(); \
	friend struct Z_Construct_UClass_AFullmetalShellPlayerController_Statics; \
public: \
	DECLARE_CLASS(AFullmetalShellPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FullmetalShell"), NO_API) \
	DECLARE_SERIALIZER(AFullmetalShellPlayerController)


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFullmetalShellPlayerController(); \
	friend struct Z_Construct_UClass_AFullmetalShellPlayerController_Statics; \
public: \
	DECLARE_CLASS(AFullmetalShellPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FullmetalShell"), NO_API) \
	DECLARE_SERIALIZER(AFullmetalShellPlayerController)


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFullmetalShellPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFullmetalShellPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFullmetalShellPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFullmetalShellPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFullmetalShellPlayerController(AFullmetalShellPlayerController&&); \
	NO_API AFullmetalShellPlayerController(const AFullmetalShellPlayerController&); \
public:


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFullmetalShellPlayerController(AFullmetalShellPlayerController&&); \
	NO_API AFullmetalShellPlayerController(const AFullmetalShellPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFullmetalShellPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFullmetalShellPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFullmetalShellPlayerController)


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_9_PROLOG
#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_SPARSE_DATA \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_RPC_WRAPPERS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_INCLASS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_SPARSE_DATA \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_INCLASS_NO_PURE_DECLS \
	FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FULLMETALSHELL_API UClass* StaticClass<class AFullmetalShellPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FullmetalShell_Source_FullmetalShell_Game_FullmetalShellPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
