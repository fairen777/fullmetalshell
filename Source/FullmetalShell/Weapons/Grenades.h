// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "FullmetalShell/FuncLibrary/Types.h"
#include "FullMetalShell/Weapons/ProjDef.h"
#include "HAL/IConsoleManager.h"
#include "Grenades.generated.h"

/**
 *
 */
UCLASS()
class FULLMETALSHELL_API AGrenades : public AProjDef
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;

	void TimerExplode(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	void Explode();

	bool TimerEnabled = false;
	float TimerToExplode = 0.0f;
	float TimeToExplode = 4.0f;

};
