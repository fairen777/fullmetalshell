// Fill out your copyright notice in the Description page of Project Settings.


#include "FullmetalShell/Weapons/Grenades.h"

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
bool ExplodeDebugShow = false;

FAutoConsoleVariableRef CVARExplodeShow(TEXT("TPS.DebugExplode"), ExplodeDebugShow, TEXT("Draw explode debug sphere"), ECVF_Cheat);

void AGrenades::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplode(DeltaTime);

}

void AGrenades::TimerExplode(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplode > TimeToExplode)
		{
			Explode();
		}
		else
		{
			TimerToExplode += DeltaTime;
		}
	}
}

void AGrenades::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AGrenades::ImpactProjectile()
{
	TimerEnabled = true;
}

void AGrenades::Explode()
{
	TimerEnabled = false;

	if (ProjectileSetting.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExplodeBaseDamage,
		ProjectileSetting.ExplodeMinDamage, GetActorLocation(), ProjectileSetting.ExplodeInnerRadius, ProjectileSetting.ExplodeOuterRadius,
		ProjectileSetting.DamageRatio, NULL, IgnoredActor, nullptr, nullptr);

    if(ExplodeDebugShow)
    {
        DrawDebugSphere(GetWorld(), GetActorLocation(),
            ProjectileSetting.ExplodeInnerRadius, 8, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);

		DrawDebugSphere(GetWorld(), GetActorLocation(),
			((ProjectileSetting.ExplodeInnerRadius + ProjectileSetting.ExplodeOuterRadius) / ProjectileSetting.DamageRatio), 8, FColor::Orange, false, 5.f, (uint8)'\000', 0.5f);

        DrawDebugSphere(GetWorld(), GetActorLocation(),
            ProjectileSetting.ExplodeOuterRadius, 8, FColor::Green, false, 5.f, (uint8)'\000', 0.5f);
    }

	this->Destroy();
}
