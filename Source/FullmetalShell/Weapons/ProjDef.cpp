// Fill out your copyright notice in the Description page of Project Settings.


#include "FullmetalShell/Weapons/ProjDef.h"

// Sets default values
AProjDef::AProjDef()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	BulletCollisionSphere->SetSphereRadius(16.f);
	BulletCollisionSphere->bReturnMaterialOnMove = true; //hit event return physMaterial
	BulletCollisionSphere->SetCanEverAffectNavigation(false); //collision not affect navigation (P keybord on editor)
	RootComponent = BulletCollisionSphere;

	BulletMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMeshComponent->SetupAttachment(RootComponent);
	BulletMeshComponent->SetCanEverAffectNavigation(false);

	ParticleTraceComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet ParticleSystem FX"));
	ParticleTraceComponent->SetupAttachment(RootComponent);

	NiagaraTraceComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Bullet NiagaraSysytem FX"));
	NiagaraTraceComponent->SetupAttachment(RootComponent);

	BulletSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	BulletSoundComponent->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjDef::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjDef::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjDef::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjDef::BulletCollisionSphereEndOverlap);

	InitProjectile(ProjectileSetting);
	SpawnLoc = this->GetActorLocation(); //GetWorldLoc?
}

// Called every frame
void AProjDef::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AProjDef::InitProjectile(FProjectileInfo InitParam)
{
	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	

	//if (InitParam.BulletMesh)
	//{
		BulletMeshComponent->SetStaticMesh(InitParam.BulletMesh);
		BulletMeshComponent->SetRelativeTransform(InitParam.BulletMeshOffset);
	//}

	//if (InitParam.BulletTraceParticleFX)
	//{
		ParticleTraceComponent->SetTemplate(InitParam.BulletTraceParticleFX);
	//}
	
	
		//ParticleTraceComponent->DestroyComponent();
		//UE_LOG(LogTemp, Warning, TEXT("Partlce Component Dectroy"))
	

	//if (InitParam.BulletTraceNiagaraFX)
	//{
		NiagaraTraceComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(InitParam.BulletTraceNiagaraFX, BulletMeshComponent, NAME_None,
			InitParam.TraceNiagaraOffset.GetLocation(), InitParam.TraceNiagaraOffset.Rotator(), EAttachLocation::Type::KeepRelativeOffset, false, true, ENCPoolMethod::None);
	//}


		//NiagaraTraceComponent->DestroyComponent();
		//UE_LOG(LogTemp, Warning, TEXT("Niagara Component Dectroy"))
	

	ProjectileSetting = InitParam;
}

void AProjDef::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
			
		if (ProjectileSetting.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];
				
			if (myMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.f);
				UE_LOG(LogTemp, Warning, TEXT("Material Hit Initialized"))
			}
		}
				
		if (ProjectileSetting.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];
			UE_LOG(LogTemp, Warning, TEXT("Partlce Initialized"))

			if (myParticle && OtherComp)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
				UE_LOG(LogTemp, Warning, TEXT("Partlce Hit Initialized"))
			}
		}

		if (ProjectileSetting.HitSound.Contains(mySurfacetype))
		{
			USoundBase* mySound = ProjectileSetting.HitSound[mySurfacetype];

			if (mySound)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), mySound, Hit.ImpactPoint);
				UE_LOG(LogTemp, Warning, TEXT("Sound Hit Initialized"))
			}
				
		}
	}
	//Bullet direction vector
	FVector TempleVector = SpawnLoc - this->GetActorLocation();
	float CurrentDistance = TempleVector.Size(); 
	float CurrentDamage = ProjectileSetting.ProjectileDamage * (CurrentDistance / ProjectileSetting.MaxDistance);
	
	
	UGameplayStatics::ApplyDamage(OtherActor, CurrentDamage, GetInstigatorController(), this, NULL);
	
	GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Orange, FString::Printf(TEXT("Projectile hit : %f "), CurrentDamage));

	ImpactProjectile();
}


void AProjDef::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjDef::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjDef::ImpactProjectile()
{
	this->Destroy();
}

