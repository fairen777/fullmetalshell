// Fill out your copyright notice in the Description page of Project Settings.


#include "FullmetalShell/Weapons/WepDef.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "FullmetalShell/Game/FSGameInstance.h"
#include "FullmetalShell/Character/InventoryComponent.h"

// Sets default values
AWepDef::AWepDef()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWepDef::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWepDef::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	MagazineDropTick(DeltaTime);
	ShellDropTick(DeltaTime);
}

void AWepDef::FireTick(float DeltaTime)
{
        if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
        {
            if (FireTimer < 0.f)
            {
					Fire();
            }
            else
            {
                FireTimer -= DeltaTime;
            }
        }
}

void AWepDef::ReloadTick(float DeltaTime)
{
    if (WeaponReloading)
    {
        if (ReloadTimer < 0.0f)
        {
            FinishReload();
        }
        else
        {
            ReloadTimer -= DeltaTime;
        }
    }
}

void AWepDef::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if(ShoudReduceDispersion)
			{
				CurrentDispersion -= CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion += CurrentDispersionReduction;
			}
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}

}

void AWepDef::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;

			InitDropMes(WeaponSetting.ShellDrop.DropMesh, WeaponSetting.ShellDrop.DropMeshOffset, WeaponSetting.ShellDrop.DropMeshImpulseDir,
				WeaponSetting.ShellDrop.DropMeshLifeTime, WeaponSetting.ShellDrop.ImpulseRandomDispersion,
				WeaponSetting.ShellDrop.PowerImpulse, WeaponSetting.ShellDrop.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWepDef::MagazineDropTick(float DeltaTime)
{
	if (DropMagazineFlag)
	{
		if (DropMagazineTimer < 0.0f)
		{
			DropMagazineFlag = false;

			InitDropMes(WeaponSetting.MagazineDrop.DropMesh, WeaponSetting.MagazineDrop.DropMeshOffset, WeaponSetting.MagazineDrop.DropMeshImpulseDir,
				WeaponSetting.MagazineDrop.DropMeshLifeTime, WeaponSetting.MagazineDrop.ImpulseRandomDispersion,
				WeaponSetting.MagazineDrop.PowerImpulse, WeaponSetting.MagazineDrop.CustomMass);
		}
		else
		{
			DropMagazineTimer -= DeltaTime;
		}
	}
}

void AWepDef::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent(true);
	}

	UpdateStateWeapon(EMovementState::Run_State);
}

void AWepDef::InitDropMes(UStaticMesh* DropMesh, FTransform Offset, float DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform TransForm;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;

		TransForm.SetLocation(GetActorLocation() + LocalDir);
		TransForm.SetScale3D(Offset.GetScale3D());
		TransForm.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		
		AStaticMeshActor* NewActor = nullptr;

		FActorSpawnParameters ActorSpawnParam;
		ActorSpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		ActorSpawnParam.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), TransForm, ActorSpawnParam);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = LifeTimeMesh;
			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);
			
			if (CustomMass>0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if (!(DropImpulseDirection <= 0))
			{
				FVector FinalDir;
				LocalDir = LocalDir.RotateAngleAxis(DropImpulseDirection, GetActorUpVector()); //LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);

				if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
				{
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
					//FinalDir.GetSafeNormal(0.0001f);
				}
				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
			}
		}
	}
}

void AWepDef::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
		//FireTimer = 0.01f; //works odd
	}

	FireTimer = 0.01f;
}

bool AWepDef::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWepDef::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWepDef::Fire()
{
	UAnimMontage* AnimToPlay = nullptr;
	switch (WeaponState)
	{
	case EMovementState::Aim_State:
		UE_LOG(LogTemp, Warning, TEXT("Play AnimCharFire_AIM"))
		AnimToPlay = WeaponSetting.WeaponAnimInfo.AnimCharFireAim;
		break;
	default:
		UE_LOG(LogTemp, Warning, TEXT("Play AnimCharFire"))
		AnimToPlay = WeaponSetting.WeaponAnimInfo.AnimCharFire;
		break;
	}

	FireTimer = WeaponSetting.RateOfFire;
	WeaponInfo.Round = WeaponInfo.Round - 1;
	ChangeDispersionByShot();

	

	if (WeaponSetting.SoundFireWeapon)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	}
	
	if (WeaponSetting.ParticleFireWeapon)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.ParticleFireWeapon, ShootLocation->GetComponentTransform());
	}

	int8 NumberProjectile = GetNumberProjectileByShoot();

	//anim character fire
	OnWeaponFireStart.Broadcast(AnimToPlay);

		UE_LOG(LogTemp, Warning, TEXT("Round = %d"), WeaponInfo.Round)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX= %f, MIN = %f, Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion)

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;

		//anim weapon shot
		if (WeaponSetting.WeaponAnimInfo.AnimWeaponFire && SkeletalMeshWeapon->SkeletalMesh)
		{
			UE_LOG(LogTemp, Warning, TEXT("Weapon anim init OK"))
			//SkeletalMeshWeapon->PlayAnimation(WeaponSetting.WeaponAnimInfo.AnimWeaponFire, false);
			SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.WeaponAnimInfo.AnimWeaponFire, 1.0f, EMontagePlayReturnType::MontageLength, 0.0f);
		}
		
		//ShellDrop flag
		if (WeaponSetting.ShellDrop.DropMesh)
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellDrop.DropMeshTime;
		}
		
		for (int i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();

			if (ProjectileInfo.Projectile)
			{
			    //Projectile Init ballistic fire

				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjDef* myProjectile = Cast<AProjDef>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Projectile not found"))

				//Weapon trace fire

				FCollisionQueryParams TraceParams(TEXT("WeaponTrace"), true, this);
				TraceParams.bReturnPhysicalMaterial = true;

				FHitResult TraceHit;
				TArray<AActor*> IgnorActors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistacneTrace, ETraceTypeQuery::TraceTypeQuery3, false, IgnorActors, EDrawDebugTrace::ForDuration, TraceHit, true, FLinearColor::Yellow, FLinearColor::Blue, 5.0f);
				
				if (TraceHit.GetActor())
				{
					GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Orange, FString::Printf(TEXT("Trace hit : % s "), *TraceHit.GetActor()->GetName()));
				}
				else
				{
					GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Orange, FString::Printf(TEXT("Trace hit : NONE")));
				}
				
				//or
				
				/*bool bHit = GetWorld()->LineTraceSingleByChannel(TraceHit, SpawnLocation, EndLocation * WeaponSetting.DistacneTrace, ECC_Visibility, TraceParams);
				DrawDebugLine(GetWorld(), SpawnLocation, EndLocation, FColor::Orange, false, 1.f, (uint8)'\000', 1.0f);

				if (bHit)
				{
					GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Orange, FString::Printf(TEXT("Trace hit : % s "), *TraceHit.GetActor()->GetName()));
				}*/

				//Trace hit logic

				if (TraceHit.GetActor() && TraceHit.PhysMaterial.IsValid())
                {

                    EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(TraceHit);

                    if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfaceType))
                    {
                        UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfaceType];

                       if (mySurfaceType && TraceHit.GetComponent())
                       {
						   UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.f), TraceHit.GetComponent(), NAME_None, TraceHit.ImpactPoint, TraceHit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition);
                       }
                    }
					
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfaceType))
					{
						UParticleSystem* myPartical = WeaponSetting.ProjectileSetting.HitFXs[mySurfaceType];
						if (myPartical)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myPartical, FTransform(TraceHit.ImpactNormal.Rotation(), TraceHit.ImpactPoint, FVector(1.0f)));
						}
					}
					
					if (WeaponSetting.ProjectileSetting.HitSound.Contains(mySurfaceType))
					{
						USoundBase* mySound = WeaponSetting.ProjectileSetting.HitSound[mySurfaceType];
						if (mySound)
						{
							UGameplayStatics::PlaySoundAtLocation(GetWorld(), mySound, TraceHit.ImpactPoint);
						}
					}

					//Apply Trace Damage
					UGameplayStatics::ApplyDamage(TraceHit.GetActor(), WeaponSetting.WeaponDamage, GetInstigatorController(), this, NULL);
					GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Orange, FString::Printf(TEXT("Projectile hit : %f "), WeaponSetting.WeaponDamage));
					
                }
			}
		}
	}

	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		//init reload
		if (CanWeaponReload())
		{
			InitReload();
		}
	}
}

void AWepDef::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;      //NOT WORK!?
		SetWeaponStateFire(false);
		break;
	}
	WeaponState = NewMovementState;
}

void AWepDef::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

float AWepDef::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWepDef::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

FVector AWepDef::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = ShootLocation->GetComponentLocation() - ShootEndLocation;

	if (tmpV.Size() > SizeShootVector)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(tmpV.GetSafeNormal()) * -20000.0f;

		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -tmpV.GetSafeNormal(), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 5.f, (uint8)'\000', 1.0f);
		}
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;

		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 5.f, (uint8)'\000', 1.0f);
		}
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), (ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*500.f), FColor::Orange, false, 5.f, (uint8)'\000', 0.5f);
		//Proper fly direction
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Current fly direction
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeShootVector, 10.f, 8, FColor::Blue, false, 5.f, (uint8)'\000', 0.5f);
	}

	return EndLocation;
}

int8 AWepDef::GetNumberProjectileByShoot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWepDef::GetWeaponRound()
{
    return WeaponInfo.Round;
}

EWeaponType AWepDef::GetTypeWeapon()
{
	return WeaponSetting.TypeWeapon;
}

EWeaponAnimType AWepDef::GetTypeAnimWeapon()
{
	return WeaponSetting.WeaponAnimInfo.CharacterIdleAnimType;
}

void AWepDef::InitReload()
{	
	if (CanWeaponReload())
	{

		WeaponReloading = true;
		BlockFire = true;

		if (!IsReloadAnimPlay)
		{
			UAnimMontage* AnimToPlay = nullptr;
			ReloadTimer = WeaponSetting.ReloadTime;
			IsReloadAnimPlay = true;

			switch (WeaponState)
			{
			case EMovementState::Aim_State:
				AnimToPlay = WeaponSetting.WeaponAnimInfo.AnimCharReloadAim;
				UE_LOG(LogTemp, Warning, TEXT("Play AnimCharReload_AIM"))
					break;
			default:
				UE_LOG(LogTemp, Warning, TEXT("Play AnimCharReload"))
					AnimToPlay = WeaponSetting.WeaponAnimInfo.AnimCharReload;
				break;
			}

			if (AnimToPlay)
			{
				UE_LOG(LogTemp, Warning, TEXT("Check AnimCharReload OK"))
					OnWeaponReloadStart.Broadcast(AnimToPlay);
			}

			if (WeaponSetting.WeaponAnimInfo.AnimWeaponReload && SkeletalMeshWeapon->SkeletalMesh)
			{
				UE_LOG(LogTemp, Warning, TEXT("Weapon reload anim play"))
					//SkeletalMeshWeapon->PlayAnimation(WeaponSetting.WeaponAnimInfo.AnimWeaponReload, false);
					SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.WeaponAnimInfo.AnimWeaponReload, 1.0f, EMontagePlayReturnType::MontageLength, 0.0f);
			}

			if (WeaponSetting.SoundReloadWeapon)
			{
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundReloadWeapon, GetActorLocation(), FRotator::ZeroRotator, 1.0f, 1.0f, WeaponSetting.ReloadSoundStartTime);
			}

			if (WeaponSetting.MagazineDrop.DropMesh)
			{
				DropMagazineFlag = true;
				DropMagazineTimer = WeaponSetting.MagazineDrop.DropMeshTime;
			}
		}
	}
}

void AWepDef::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd.Broadcast(false, 0);
	DropMagazineFlag = false;
}

bool AWepDef::CanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UInventoryComponent* MyInv = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int8 AviableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.TypeWeapon, AviableAmmoForWeapon))
			{
				result = false;
			}

		}
	}

	return result;
}

int8 AWepDef::GetAvailableAmmoForReload()
{
	int8 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UInventoryComponent* MyInv = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.TypeWeapon, AviableAmmoForWeapon))
			{
				AviableAmmoForWeapon = AviableAmmoForWeapon;
			}

		}
	}
	return AviableAmmoForWeapon;
}

void AWepDef::FinishReload()
{
	
    WeaponReloading = false;
	int8 AvailableAmmoFromInventory = GetAvailableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - WeaponInfo.Round;

	if (NeedToReload > AvailableAmmoFromInventory)
	{
		WeaponInfo.Round = AvailableAmmoFromInventory;
		AmmoNeedTakeFromInv = AvailableAmmoFromInventory;
	}
	else
	{
		WeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	IsReloadAnimPlay = false;
	BlockFire = false;

	OnWeaponReloadEnd.Broadcast(true, AmmoNeedTakeFromInv);
}

