// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FullmetalShellGameMode.generated.h"

UCLASS(minimalapi)
class AFullmetalShellGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFullmetalShellGameMode();
};



