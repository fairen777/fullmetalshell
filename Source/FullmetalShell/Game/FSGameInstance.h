// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FullmetalShell/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "FullmetalShell/Weapons/WepDef.h"
#include "FSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class FULLMETALSHELL_API UFSGameInstance : public UGameInstance
{
	GENERATED_BODY()


public:
	//table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* WeaponInfoTable = nullptr;

	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);

};
