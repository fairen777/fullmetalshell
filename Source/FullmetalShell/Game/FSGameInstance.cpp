// Fill out your copyright notice in the Description page of Project Settings.


#include "FullmetalShell/Game/FSGameInstance.h"

bool UFSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool IsFindRow = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
        WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, " ", false);
        if (WeaponInfoRow)
        {
            IsFindRow = true;
            OutInfo = *WeaponInfoRow;
        }
	}
	else
    {
        UE_LOG(LogTemp, Warning, TEXT("UFSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
    }

    return IsFindRow;
}
