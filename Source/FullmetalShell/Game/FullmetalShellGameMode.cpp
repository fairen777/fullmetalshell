// Copyright Epic Games, Inc. All Rights Reserved.

#include "FullmetalShellGameMode.h"
#include "FullmetalShell/Game/FullmetalShellPlayerController.h"
#include "FullmetalShell/Character/FullmetalShellCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFullmetalShellGameMode::AFullmetalShellGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AFullmetalShellPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}