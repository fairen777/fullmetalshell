// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FullmetalShell : ModuleRules
{
	public FullmetalShell(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {"Core", "CoreUObject", "Engine", "InputCore", "UMG", "HeadMountedDisplay", "NavigationSystem", "AIModule", "Slate", "PhysicsCore", "Niagara"});
    }
}
