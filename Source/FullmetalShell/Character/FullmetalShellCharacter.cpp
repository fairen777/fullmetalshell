// Copyright Epic Games, Inc. All Rights Reserved.

#include "FullmetalShell/Character/FullmetalShellCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "FullmetalShell/Game/FSGameInstance.h"
#include "Engine/World.h"

AFullmetalShellCharacter::AFullmetalShellCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a inventory...
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AFullmetalShellCharacter::InitWeapon);
	}

	// Create a decal in the world to show the cursor's location
	/*CurrentCursor = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CurrentCursor->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CurrentCursor->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CurrentCursor->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CurrentCursor->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	*/


	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AFullmetalShellCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/

	if (CurrentCursor)
	{
		APlayerController* PC = Cast<APlayerController>(GetController());

		if (PC)
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void AFullmetalShellCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName, );

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void AFullmetalShellCharacter::SetupPlayerInputComponent (UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent (PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AFullmetalShellCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AFullmetalShellCharacter::InputAxisY);
	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AFullmetalShellCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AFullmetalShellCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &AFullmetalShellCharacter::TryReloadWeapon);
	PlayerInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AFullmetalShellCharacter::TrySwitchNextWeapon);
	PlayerInputComponent->BindAction(TEXT("SwitchPrevWeapon"), EInputEvent::IE_Pressed, this, &AFullmetalShellCharacter::TrySwitchPrevWeapon);
}

void AFullmetalShellCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void AFullmetalShellCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void AFullmetalShellCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void AFullmetalShellCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AFullmetalShellCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult HitResult;
		myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);
		GetActorForwardVector().ForwardVector;

		if (IsPawnRotationON)
		{
			float FindYawRotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindYawRotator, 0.0f)));
		}

		if (CurrentWeapon) //replace ShouldReduceDispersion here? or not? L7.6
        {
            FVector Displasment = FVector(0);
            switch (MovementState)
            {
            case EMovementState::Aim_State:
				CurrentWeapon->WeaponState = EMovementState::Aim_State;
                Displasment = FVector(0.0f, 0.0f, 160.0f);
                    break;
            case EMovementState::Walk_State:
                CurrentWeapon->WeaponState = EMovementState::Walk_State;
                Displasment = FVector(0.0f, 0.0f, 80.0f);
                    break;
            case EMovementState::Run_State:
                CurrentWeapon->WeaponState = EMovementState::Run_State;
                Displasment = FVector(0.0f, 0.0f, 120.0f);
                    break;
            case EMovementState::Sprint_State:
                CurrentWeapon->WeaponState = EMovementState::Sprint_State;
				Displasment = FVector(0.0f, 0.0f, 200.0f);
                break;
            }
            CurrentWeapon->ShootEndLocation = HitResult.Location + Displasment;
            //cursor like 3D widget?
        }
	}

	if (CurrentWeapon)
	{
		if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
			CurrentWeapon->ShoudReduceDispersion = true;
		else
			CurrentWeapon->ShoudReduceDispersion = false;
	}
}

void AFullmetalShellCharacter::AttackCharEvent(bool bIsFiring)
{
	AWepDef* MyWeapon = GetCurrentWeapon();

	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("FMSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}

}

void AFullmetalShellCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
			break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
			break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
			break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
            break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AFullmetalShellCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;

	if (MovementState == EMovementState::Sprint_State)
	{
		if (PlayerStamina<=0)
		{
			MovementState = EMovementState::Run_State;
		}

	}

	CharacterUpdate();

	AWepDef* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void AFullmetalShellCharacter::StaminaChangeFunc()
{
	if (MovementState == EMovementState::Sprint_State && !GetVelocity().Equals(FVector(0.0f, 0.0f, 0.0f)))
	{
		if (PlayerStamina >= 0.1f)
		{
			PlayerStamina -= 0.1f;
		}
		else
		{
			MovementState = EMovementState::Run_State;
			GetCharacterMovement()->MaxWalkSpeed = 600.f;
		}
	}

	else if (PlayerStamina < 1)
	{
		PlayerStamina += 0.1f;
	}
}

AWepDef* AFullmetalShellCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void AFullmetalShellCharacter::InitWeapon(FName IDWeapon, FAddicionalWeaponInfo AdditWeapInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UFSGameInstance* myGI = Cast<UFSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;

	if (myGI)
	{
		if(myGI->GetWeaponInfoByName(IDWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWepDef* myWeapon = Cast<AWepDef>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));

				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					
					//Debug
					myWeapon->ReloadTimer = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->WeaponInfo = AdditWeapInfo;
					if (InventoryComponent)
					{
						CurrentIndex = InventoryComponent->GetWeaponIndexSlotByName(IDWeapon);
					}

					//Delegate
					myWeapon->OnWeaponFireStart.AddDynamic(this, &AFullmetalShellCharacter::WeaponFireStart);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AFullmetalShellCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AFullmetalShellCharacter::WeaponReloadEnd);
				
					//auto reload after switch weapon
					if (CurrentWeapon->GetWeaponRound() <=0 && CurrentWeapon->CanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}
					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->GetTypeWeapon());
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AFSCharacter::InitWeapon - Weapon not foundin table -NULL"))
		}
	}
}

void AFullmetalShellCharacter::TryReloadWeapon()
{
    if (CurrentWeapon) //&& !CurrentWeapon->WeaponReloding fix reloding
    {
        if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound)
        {
            CurrentWeapon->InitReload();
        }
    }
}

void AFullmetalShellCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndex;
		FAddicionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;

			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{

			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndex + 1, OldIndex, OldInfo, true))
			{

			}
		}
	}
}

void AFullmetalShellCharacter::TrySwitchPrevWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndex;
		FAddicionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;

			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{

			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndex - 1, OldIndex, OldInfo, false))
			{

			}
		}
	}
}


void AFullmetalShellCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndex, CurrentWeapon->WeaponInfo);
	}

	WeaponFireStart_BP(Anim);
}

void AFullmetalShellCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void AFullmetalShellCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{

	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndex, CurrentWeapon->WeaponInfo);
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.TypeWeapon, AmmoSafe);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void AFullmetalShellCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void AFullmetalShellCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void AFullmetalShellCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//in BP
}

UDecalComponent* AFullmetalShellCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}


