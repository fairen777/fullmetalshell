// Copyright Epic Games, Inc. All Rights Reserved.

#include "FullmetalShell.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FullmetalShell, "FullmetalShell" );

DEFINE_LOG_CATEGORY(LogFullmetalShell)